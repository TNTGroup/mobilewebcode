﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using System.Web;
//using System.Web.Mvc;
using System.Text;
using System.Threading.Tasks;
using MobileStoreBLL.ViewModel;

namespace MobileStoreBLL.BLL
{
    public class SanPhamBLL
    {
        private MOBILESTOREEntities db = new MOBILESTOREEntities();

        //
        // GET: /SanPham/

        public List<SanPham> List()
        {
            var sanphams = db.SanPhams.Include(s => s.HangSanXuat).Include(s => s.LoaiSanPham).Include(s => s.TinhTrangSanPham);
            return sanphams.ToList();
        }

        //
        // GET: /SanPham/Details/5

        public SanPham Details(int id)
        {
            //SanPham sp = db.SanPhams.Find(id);
            //var dsBL = from bl in db.BinhLuans
            //           where bl.SanPham.sp_id == id
            //           select bl;
            //sp.BinhLuans = dsBL.ToList();
            return db.SanPhams.Find(id);
        }

        //
        // GET: /SanPham/Create

        public ChiTietSanPham Create()
        {
            //ViewBag.sp_producer = new SelectList(db.HangSanXuats, "hsx_id", "hsx_name");
            //ViewBag.sp_type = new SelectList(db.LoaiSanPhams, "lsp_id", "lsp_name");
            //ViewBag.sp_status = new SelectList(db.TinhTrangSanPhams, "tt_id", "tt_name");
            ChiTietSanPham ctsp = new ChiTietSanPham();
           
            ctsp.DSHangSanXuat = db.HangSanXuats.ToList();
            ctsp.DSLoaiSanPham = db.LoaiSanPhams.ToList();
            ctsp.DSTinhTrangSanPham = db.TinhTrangSanPhams.ToList();
            return ctsp;
        }

        //
        // POST: /SanPham/Create


        public void  Create(SanPham sanpham)
        {
            db.SanPhams.Add(sanpham);
            db.SaveChanges();
        }

        //    ViewBag.sp_producer = new SelectList(db.HangSanXuats, "hsx_id", "hsx_name", sanpham.sp_producer);
        //    ViewBag.sp_type = new SelectList(db.LoaiSanPhams, "lsp_id", "lsp_name", sanpham.sp_type);
        //    ViewBag.sp_status = new SelectList(db.TinhTrangSanPhams, "tt_id", "tt_name", sanpham.sp_status);
        //    return View(sanpham);
        //}

        //

        // POST: /SanPham/Edit/5
        public bool Exist(int id)
        {
            return (db.SanPhams.Any(s => s.sp_id == id));
        }

     
        public void Eidit(SanPham sanpham)
        {
            db.Entry(sanpham).State = EntityState.Modified;
            db.SaveChanges();
        }

        //
        // GET: /SanPham/Delete/5

        public void Delete(int id = 0)
        {
            SanPham sanpham = db.SanPhams.Find(id);
            db.SanPhams.Remove(sanpham);
            db.SaveChanges(); 
        }

        //
        // POST: /SanPham/Delete/5
    }
}
