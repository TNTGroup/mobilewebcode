﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileStoreBLL.BLL
{
    public class CommentBLL
    {
        private MOBILESTOREEntities db = new MOBILESTOREEntities();

        public List<BinhLuan> ToList(int sp_id)
        {
            var dsBL = from bl in db.BinhLuans
                       where bl.SanPham.sp_id == sp_id
                       select bl;
            return (dsBL.ToList());
        }
        public void Add(int id, string content, string userName)
        {
            BinhLuan bl = new BinhLuan();
            bl.bl_sanPham = id;
            bl.bl_content = content;
            bl.bl_name = userName;
            bl.bl_datetime = DateTime.Now;
            db.BinhLuans.Add(bl);
            db.SaveChanges();
        }

        public void Add(BinhLuan bl)
        {
            db.BinhLuans.Add(bl);
            db.SaveChanges();
        }
    }
}
