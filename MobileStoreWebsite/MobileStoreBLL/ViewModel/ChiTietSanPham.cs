﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileStoreBLL.ViewModel
{
    public class ChiTietSanPham
    {
        public SanPham SanPham { get; set; }
        public List<BinhLuan> DSBinhLuans { set; get; }
        public List<LoaiSanPham> DSLoaiSanPham { set; get; }
        public List<HangSanXuat> DSHangSanXuat { set; get; }
        public List<TinhTrangSanPham> DSTinhTrangSanPham { set; get; }
    }
}
