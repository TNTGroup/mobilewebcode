﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileStoreBLL;
using MobileStoreBLL.BLL;
using MobileStoreBLL.ViewModel;

namespace MobileSoreWebsite.Controllers
{
    public class SanPhamController : Controller
    {
        private SanPhamBLL spBLL = new SanPhamBLL();
        private CommentBLL cmBLL = new CommentBLL();
        //
        // GET: /SanPham/

        public ActionResult Index()
        {
            //var sanphams = db.SanPhams.Include(s => s.HangSanXuat).Include(s => s.LoaiSanPham).Include(s => s.TinhTrangSanPham);
            return View(spBLL.List());
        }

        //
        // GET: /SanPham/Details/5

        public ActionResult Details(int id = 0)
        {
            SanPham sanpham = spBLL.Details(id);
            sanpham.BinhLuans = cmBLL.ToList(id);
            return View(sanpham);
        }

        //
        // GET: /SanPham/Create

        public ActionResult Create()
        {
            ChiTietSanPham ctsp= spBLL.Create();
            ViewBag.sp_producer = new SelectList(ctsp.DSHangSanXuat, "hsx_id", "hsx_name");
            ViewBag.sp_type = new SelectList(ctsp.DSLoaiSanPham, "lsp_id", "lsp_name");
            ViewBag.sp_status = new SelectList(ctsp.DSTinhTrangSanPham, "tt_id", "tt_name");
            return View();
        }

        //
        // POST: /SanPham/Create

        [HttpPost]
        public ActionResult Create(SanPham sanpham)
        {
            if (ModelState.IsValid)
            {
                spBLL.Create(sanpham);
                return RedirectToAction("Index");
            }

            ChiTietSanPham ctsp = spBLL.Create();
            ViewBag.sp_producer = new SelectList(ctsp.DSHangSanXuat, "hsx_id", "hsx_name");
            ViewBag.sp_type = new SelectList(ctsp.DSLoaiSanPham, "lsp_id", "lsp_name");
            ViewBag.sp_status = new SelectList(ctsp.DSTinhTrangSanPham, "tt_id", "tt_name");
            return View(sanpham);
        }

        //
        // GET: /SanPham/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SanPham sanpham = spBLL.Details(id);
            if (sanpham == null)
            {
                return HttpNotFound();
            }
            ChiTietSanPham ctsp = spBLL.Create();
            ViewBag.sp_producer = new SelectList(ctsp.DSHangSanXuat, "hsx_id", "hsx_name");
            ViewBag.sp_type = new SelectList(ctsp.DSLoaiSanPham, "lsp_id", "lsp_name");
            ViewBag.sp_status = new SelectList(ctsp.DSTinhTrangSanPham, "tt_id", "tt_name");
            return View(sanpham);
        }

        //
        // POST: /SanPham/Edit/5

        [HttpPost]
        public ActionResult Edit(SanPham sanpham)
        {
            if (ModelState.IsValid)
            {
                spBLL.Eidit(sanpham);
            }
            ChiTietSanPham ctsp = spBLL.Create();
            ViewBag.sp_producer = new SelectList(ctsp.DSHangSanXuat, "hsx_id", "hsx_name");
            ViewBag.sp_type = new SelectList(ctsp.DSLoaiSanPham, "lsp_id", "lsp_name");
            ViewBag.sp_status = new SelectList(ctsp.DSTinhTrangSanPham, "tt_id", "tt_name");
            return RedirectToAction("Index");// View(sanpham);
        }

        [HttpPost]
        public ActionResult UpdateComment(string user, string content, int id)
        {
            //cmBLL.Add(id, content, user); 
            BinhLuan bl = new BinhLuan();
            bl.bl_name  = user;
            bl.bl_sanPham = id;
            bl.bl_content = content;
            bl.bl_datetime = DateTime.Now;
            cmBLL.Add(bl);
            return Json("binh luan da luu tru",JsonRequestBehavior.AllowGet);
        }
        //
        // GET: /SanPham/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SanPham sanpham = spBLL.Details(id);
            if (sanpham == null)
            {
                return HttpNotFound();
            }
            return View(sanpham);
        }

        //
        // POST: /SanPham/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            spBLL.Delete(id);
            return RedirectToAction("Index");
        }
    }
}