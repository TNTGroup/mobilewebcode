﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileStoreBLL;
using MobileStoreBLL.BLL;
using System.Data.Entity;
using MobileStoreBLL.ViewModel;

namespace MobileSoreWebsite.Controllers
{
    public class SearchController : Controller
    {
        private MOBILESTOREEntities db = new MOBILESTOREEntities();
        SanPhamBLL spBLL = new SanPhamBLL();
        public ActionResult Index()
        {
            ChiTietSanPham ctsp = spBLL.Create();
            ViewBag.sp_producer = new SelectList(ctsp.DSHangSanXuat, "hsx_id", "hsx_name");
            ViewBag.sp_type = new SelectList(ctsp.DSLoaiSanPham, "lsp_id", "lsp_name");
            ViewBag.sp_status = new SelectList(ctsp.DSTinhTrangSanPham, "tt_id", "tt_name");
            return View();
        }

        
        public ActionResult Result(string sp_type, string sp_status, string sp_producer, string sp_highestPrice, string sp_lowestPrice, string keyWord)
        {
            bool Btype, Bstatus, Bproducer, BhighestPrice, BlowestPrice;
            BlowestPrice = !String.IsNullOrEmpty(sp_lowestPrice);
            BhighestPrice = !String.IsNullOrEmpty(sp_highestPrice);
            Btype = !String.IsNullOrEmpty(sp_type);
            Bstatus = !String.IsNullOrEmpty(sp_status);
            Bproducer = !String.IsNullOrEmpty(sp_producer);
                        
            int type = 0, status = 0, producer = 0, highestPrice = 0, lowestPrice = 0;
            if (Btype)
                type = Int32.Parse(sp_type);
            if (Bstatus)
                status = Int32.Parse(sp_status);
            if (Bproducer)
                producer = Int32.Parse(sp_producer);
            if (BhighestPrice)
                highestPrice = Int32.Parse(sp_highestPrice);
            if (BlowestPrice)
                lowestPrice = Int32.Parse(sp_lowestPrice);

            var q = from s in db.SanPhams.Include(s => s.HangSanXuat).Include(s => s.LoaiSanPham).Include(s => s.TinhTrangSanPham)
                    select s;

            if (Btype)
            {
                var tempP = from s in q
                            where s.sp_type == type
                            select s;
                q = tempP;
            }
           
            if (Bstatus)
            {
                var tempP = from s in q
                            where s.sp_status == status
                            select s;
                q = tempP;
            }

            if (Bproducer)
            {
                var tempP = from s in q
                            where s.sp_producer == producer
                            select s;
                q = tempP;
            }

            if (BhighestPrice)
            {
                var tempP = from s in q
                            where s.sp_price <= highestPrice
                            select s;
                q = tempP;
            }

            if (BlowestPrice)
            {
                var tempP = from s in q
                            where s.sp_price >= lowestPrice
                            select s;
                q = tempP;
            }

            if (!String.IsNullOrEmpty(keyWord))
            {
                q = q.Where(s => s.sp_name.ToUpper().Contains(keyWord.ToUpper())
                                       || s.sp_desription.ToUpper().Contains(keyWord.ToUpper()));
            }

            return View(q.ToList());
        }
    }
}
